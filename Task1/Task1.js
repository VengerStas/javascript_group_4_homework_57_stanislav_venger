const tasks = [
    {id: 234, title: 'Create user registration API', timeSpent: 4, category: 'Backend', type: 'task'},
    {id: 235, title: 'Create user registration UI', timeSpent: 8, category: 'Frontend', type: 'task'},
    {id: 237, title: 'User sign-in via Google UI', timeSpent: 3.5, category: 'Frontend', type: 'task'},
    {id: 238, title: 'User sign-in via Google API', timeSpent: 5, category: 'Backend', type: 'task'},
    {id: 241, title: 'Fix account linking', timeSpent: 5, category: 'Backend', type: 'bug'},
    {id: 250, title: 'Fix wrong time created on new record', timeSpent: 1, category: 'Backend', type: 'bug'},
    {id: 262, title: 'Fix sign-in failed messages', timeSpent: 2, category: 'Frontend', type: 'bug'},
];

const frontendTotalTime = tasks.reduce((acc, item) => {
    if (item.category === 'Frontend') acc += item.timeSpent;
    return acc;
}, 0);

const bugTotalTime = tasks.reduce((acc, item) => {
    if (item.type === 'bug') acc += item.timeSpent;
    return acc;
}, 0);

const totalUI = tasks.filter((item) => item.title.includes('UI')).length;

const frontBack = tasks.reduce((acc, item) => {
    if (item.category === 'Frontend') acc.Frontend++;
    else if (item.category === 'Backend') acc.Backend++;
    return acc;
}, {Frontend:0, Backend: 0});

const checkTime = tasks.map((item, index) => {
    if (item.timeSpent >= 4 ) {
        return {
            title: item.title,
            category: item.category
        }
    }
}).filter(item => {
    if(item !== undefined) {
        return item
    }
});

console.log('Total spent time for Frontend: ' + frontendTotalTime + ' hours');
console.log('Total spent for fixing bugs: ' + bugTotalTime + ' hours');
console.log('Total words with UI: ' + totalUI);
console.log('Frontend and Backend: ' , frontBack);
console.log('Spent more than 4 hours: ' , checkTime);

