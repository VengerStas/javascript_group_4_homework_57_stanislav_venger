import React, { Component } from 'react';
import './App.css';
import FormItems from "./components/FinancialForm/FinancialForm";
import Controls from "./components/FormControls/FormControls";

class App extends Component {
    state = {
        newItem: '',
        itemCost: '',
        items: [],
        currentItem: 0,
        totalSpent: 0
    };

    changeItemHandler = event => {
        this.setState({newItem: event.target.value});
    };

    changeCostHandler = event => {
        this.setState({itemCost: event.target.value})
    };

    addItem = () => {
        if (this.state.newItem !== '' && this.state.itemCost !== '' && this.state.itemCost >= 0) {
            const items = [...this.state.items];
            let totalSpent = this.state.totalSpent;
            const newItem = {name: this.state.newItem, cost: this.state.itemCost, id: this.state.currentItem + 1};
            items.push(newItem);

            totalSpent += parseInt(this.state.itemCost);

            this.setState({items: items, currentItem: this.state.currentItem + 1, totalSpent, newItem: '', itemCost: ''});
        } else {
            return false;
        }
    };

    deleteItem = (id, cost) => {
        const items = [...this.state.items];
        let totalSpent = this.state.totalSpent;
        const index = items.findIndex(item => {
            return (
                item.id === id
            )
        });
        totalSpent -= parseInt(cost);
        items.splice(index, 1);

        this.setState({items: items, currentItem: this.state.currentItem - 1, totalSpent});
    };
  render() {
      let list  = this.state.items.map((item, index) => {
          return (
              <div className="list-block" key = {index}>
                  <FormItems
                      name = {item.name}
                      cost = {item.cost}
                      removeHandler = {() => this.deleteItem(item.id, item.cost)}
                  />
              </div>
          )
      });
    return (
      <div className="App">
          <Controls
              addHandler={() => this.addItem()}
              changeItemHandler={event => this.changeItemHandler(event)}
              changeCostHandler={event => this.changeCostHandler(event)}
              value={this.state.newItem}
              valueCost={this.state.itemCost}
          />
          {list}
          <p className="cost">Total Spent: {this.state.totalSpent} KGS</p>
      </div>
    );
  }
}

export default App;
