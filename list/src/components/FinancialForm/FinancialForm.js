import React from 'react';
import './FinancialForm.css';

const FormItems = props => {
    return (
        <div className="list">
            <p className="item-name">{props.name}</p>
            <p className="price">{props.cost} KGS</p>
            <button onClick={props.removeHandler} className="delete-btn">X</button>
        </div>
    )
};

export default FormItems;