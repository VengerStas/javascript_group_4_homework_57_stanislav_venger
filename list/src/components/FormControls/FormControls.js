import React from 'react';
import './FormControls.css';

const Controls = props => {
    return (
        <div className="header">
            <input type="text" value={props.value} onChange={props.changeItemHandler} className="input-text" placeholder="Item name"/>
            <input type="text" value={props.valueCost} onChange={props.changeCostHandler} className="input-cost" placeholder="Cost"/>
            <span>KGS</span>
            <button onClick={props.addHandler} className="add-btn">Add</button>
        </div>
    )
};

export default Controls;